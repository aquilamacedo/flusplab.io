---
title:  "ULXrandR"
image:  "ulxrandr"
ref: "ulxrandr"
categories: "projects.md"
id: 3
---

# Description

Friendly interface for xrandr (unofficial LXDE LXrandR fork with bug fixes)

# recommendations

* C
* Regex
* xrandr

# URLs

* [ULXrandR Repository](https://github.com/giulianobelinassi/ULXrandR)
