---
title:  "Gold"
image:  "gold"
ref: "gold"
categories: "brochure.md"
id: 1
---

# Value: R$500

# Benefits

* Identification of the sponsoring brand in all event material: online and offline.
* Logo on the badge.
* Acknowledgment of the sponsor in all activities related to the event.
